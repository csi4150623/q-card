import { provide, ref } from "vue";
import { colors, useQuasar } from "quasar";
import { useRoute, useRouter } from "vue-router";
import Filters from "../../../components/Filters.vue";
import Pagination from "../../../components/Pagination.vue";
import MainDialog from "../../../components/MainDialog.vue";
import { ToggleMainDialogState } from "../../../composables/Triggers.js";
import MobileFilter from "../../../components/MobileFilter.vue";
import DeleteTable from "../components/DeleteTable.vue";
import UserCard from "../components/UserCard.vue";

import { SearchList } from "../../../composables/Search";
import {
  FetchORNumbers,
  ORNumberDetails,
  GetTableList,
  DeleteORNumber,
} from "../../../composables/TableList";

export default {
  components: {
    Filters,
    Pagination,
    MainDialog,
    DeleteTable,
    MobileFilter,
    UserCard,
  },
  setup() {
    const $q = useQuasar();
    const route = useRoute();
    const router = useRouter();

    // For table row
    let row = ref();
    // For page loading
    let pageLoadingState = ref(false);

    // For pagination
    let pagination = ref({
      sortBy: "desc",
      descending: false,
      page: 1,
      rowsPerPage: 10,
    });

    // selecting in GRADE LEVEL
    let gradelvlOptions = ["Grade-12", "Grade-11"];

    let statusOptions = ["Enrolled", "Not Enrolled"];

    // For table column in course
    // let strandOptions = ["STEM", "ABM", "HUMMS", "GAS", "CP", "CSS"];

    let columns = [
      {
        name: "f_name",
        required: true,
        label: "Name",
        align: "left",
        field: "f_name",
        sortable: true,
      },
      {
        name: "gradelvl_type",
        label: "Grade level",
        align: "left",
        field: "gradelvl_type",
      },
      {
        name: "strand_name",
        align: "left",
        label: "Strand",
        field: "strand_name",
      },
      {
        name: "section_name",
        align: "left",
        label: "Section",
        field: "section_name",
      },
      {
        name: "contact_number",
        align: "left",
        label: "Contact",
        field: "contact_number",
      },
      {
        name: "date",
        align: "left",
        label: "Date",
        field: "date",
      },

      {
        name: "address",
        align: "left",
        label: "Address",
        field: "address",
      },
      {
        name: "status_type",
        align: "left",
        label: "Status",
        field: "status_type",
      },
      {
        name: "action",
        align: "left",
        label: "Action",
        field: "action",
        style: "width: 100px",
      },
    ];
    const editORNumber = (row) => {
      console.log("rows: ", row.id);
      router.push({
        path: "add-table-list",
        query: { id: row.id }, // Pass the row ID as a query parameter
      });
    };

    const onRowClick = (event, props) => {
      // console.log(ORNumberDetails)
      const rowId = props.id;
      router.push({
        path: "view-table-list",
        query: { id: rowId }, // Pass the row ID as a query parameter
      });
    };

    FetchORNumbers().then((response) => {
      ORNumberDetails.value = response;
      console.log(response);
    });
    // Delete function
    let ids = ref();
    const deleteORNumber = (row) => {
      ids.value = row;
      ToggleMainDialogState();
    };

    const deleteOR = () => {
      ids.value = ids.value - 1;
      const indexToDelete = ids.value;
      DeleteORNumber({
        id: ORNumberDetails.value[indexToDelete].id,
      }).then((response) => {
        if (response) {
          let status = true;
          console.log("response: ", response);
          $q.notify({
            position: $q.screen.width < 767 ? "top" : "bottom-right",
            classes: `${
              status ? "onboarding-success-notif" : "onboarding-error-notif"
            } q-px-lg q-pt-none q-pb-none`,
            html: true,
            message: status
              ? `<div class="text-bold">Success!</div> Data has been Deleted.`
              : `<div class="text-bold">Failed!</div> Something went wrong.`,
          });
        }
      });
      ToggleMainDialogState();
      ORNumberDetails.value.splice(indexToDelete, 1);
    };

    provide("deleteOR", deleteOR);
    // RETURN FUNCTION
    return {
      pagination,
      gradelvlOptions,
      statusOptions,
      columns,
      editORNumber,
      pageLoadingState,
      deleteOR,
      onRowClick,
      row,
      ORNumberDetails,
      deleteORNumber,
      GetTableList,
    };
  },
};
